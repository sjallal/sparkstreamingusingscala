# Spark Streaming Tutorial

`Keeping a initial setup of the project in branch: "initial"`

- Will send the words through terminal using netcat tcp socket
  - For windows, use:
    > ncat -lk 9999
  - For mac, use:
    > nc -lk 9999

- Create a Scala Project using the IDE with 
  - sbt 1.8.2(latest)
  - jdk 11
  - scala 2.12.12

### build.sbt file:-
```sbt
name := "SparkScalaCourse"

version := "0.1"

scalaVersion := "2.12.12"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "3.0.0",
  "org.apache.spark" %% "spark-sql" % "3.0.0",
  "org.apache.spark" %% "spark-streaming" % "3.0.0"
)
```

- Add log4j.properties file.

- Add a VM option in Run > Edit Configurations > Add VM options
    > -Dlog4j.configuration=file:log4j.properties


- Tutorial Link: https://www.youtube.com/watch?v=E_hpyO-0NoM
